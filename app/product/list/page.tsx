"use client"

import ProductList from '@/components/ProductList/ProductList'
import styles from './page.module.scss'
import client from '@/graphql/apolloClient'
import { ApolloProvider } from '@apollo/client'

const ProductListPage = () => {
  return (
    <ApolloProvider client={client}>
      <ProductList />
    </ApolloProvider>
  )
}

export default ProductListPage