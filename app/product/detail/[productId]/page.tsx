"use client"

import client from '@/graphql/apolloClient';
import { ApolloProvider } from '@apollo/client';
import styles from './page.module.scss'
import ProductDetail from '@/components/ProductDetail/ProductDetail';

interface ProductDetailParams {
  params: {
    productId: string;
  }
}

const ProductDetailPage = ({ params: { productId } }: ProductDetailParams) => {
  return (
    <ApolloProvider client={client}>
      {productId && productId !== "null" ?
          <ProductDetail productId={productId} /> :
          <span className={styles.error}>No Product ID in URL!</span>}
    </ApolloProvider>
  )
}

export default ProductDetailPage;
