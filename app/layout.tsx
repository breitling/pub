import '@fontsource/public-sans';
import TopBar from '@/components/TopBar/TopBar'
import './globals.scss'
import type { Metadata } from 'next'
import Head from 'next/head'

export const metadata: Metadata = {
  title: 'Create Next App',
  description: 'Generated by create next app',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <Head>
        <title>Home App</title>
      </Head>
      <body>
        <div className="container">
          <header><TopBar /></header>
          {children}
          <footer><img src="/breitling-inline.svg" alt="Breitling logo" />© 2023 Breitling, All rights reserved.</footer>
        </div>
      </body>
    </html>
  )
}
