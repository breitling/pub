import { gql } from "@apollo/client";

export const PRODUCTS_QUERY = gql`
  query ProductCollection($first: Int, $after: String, $before: String, $filter: ProductFilterInput, $sortBy: ProductOrder, $channel: String!) {
    products(first: $first, after: $after, before: $before, channel: $channel, filter: $filter, sortBy: $sortBy) {
      edges {
        node {
          id
          slug
          name
          thumbnail {
            url
            alt
          }
          attributes {
            attribute {
              slug
            }
            values {
              name
            }
          }
          pricing {
            priceRange {
              start {
                gross {
                  amount
                  currency
                }
              }
              stop {
                gross {
                  amount
                  currency
                }
              }
            }
          }
        }
        cursor
      }
      totalCount
    }
  }
`;

export const PRODUCT_DETAILS_QUERY = gql`
  query ProductDetails($productId: ID!) {
    product(channel: "default-channel", id: $productId) {
      id
      name
      images {
        url
      }
      variants {
        id
        pricing {
          price {
            gross {
              currency
              amount
            }
          }
        }
        name
      }
    }
  }
`;
