import { ApolloClient, InMemoryCache } from "@apollo/client";

const client: ApolloClient<any> = new ApolloClient({
  uri: "https://demo.saleor.io/graphql/",
  cache: new InMemoryCache(),
});

export default client;
