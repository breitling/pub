"use client"

import Switch from '@mui/joy/Switch';
import ProductCard from "../ProductCard/ProductCard";
import { useQuery } from "@apollo/client";
import { Product } from "@/models/product";
import { PRODUCTS_QUERY } from "@/graphql/queries";
import { useEffect, useState } from "react";
import styles from './ProductList.module.scss'
import Button from '@mui/joy/Button';
import Autocomplete from '@mui/joy/Autocomplete';
import Input from '@mui/joy/Input';

const initialFilters = {
  format: [],
  artist: [],
  country: [],
  genre: [],
  label: [],
  releaseType: [],
  stockAvailability: false,
  search: "",
};

const options = {
  format: ["cd", "vinyl"],
  artist: ["abba", "acdc"],
  country: ["sweden", "uk"],
  genre: ["pop", "rock"],
  label: ["polar", "universal"],
  releaseType: ["album", "single"],
};

const ProductList = () => {
  const savedFilters = typeof window !== 'undefined' ? localStorage.getItem('filters') : null;
  const initialFiltersState = savedFilters ? JSON.parse(savedFilters) : initialFilters;
  const [isInitialLoad, setIsInitialLoad] = useState(true);
  const [filters, setFilters] = useState(initialFiltersState);
  const [after, setAfter] = useState(null);
  const [fetchedProducts, setFetchedProducts] = useState<Product[]>([]);
  const itemsPerPage = 16;

  const handleFilterChange = (filterName: string, values: any) => {
    setAfter(null);
    setFetchedProducts([]);
    setFilters((prevFilters: any) => ({ ...prevFilters, [filterName]: values }));
  };

  const handleClearFilters = async () => {
    if (filters != initialFilters) {
      setIsInitialLoad(true);
      setAfter(null);
      setFetchedProducts([]);
      setFilters(initialFilters);
    }
  }

  const getFetchQuery = () => {
    return {
      variables: {
        first: itemsPerPage,
        after,
        channel: "default-channel",
        sortBy: { direction: "ASC", field: "NAME" },
        filter: {
          search: filters.search,
          attributes: Object.entries(filters).reduce(
            (acc: { slug: string; values: string[] }[], [slug, values]) => {
              if (Array.isArray(values) && values.length > 0) {
                acc.push({ slug, values });
              }
              return acc;
            },
            []
          ),
          stockAvailability: filters.stockAvailability ? "IN_STOCK" : null,
        },
      },
    }
  }

  const setDataAfterFetch = (data: any) => {
    const products = data.products.edges.map(({ node }: { node: any }) => {
      const artistAttribute = node.attributes.find((attribute: any) => attribute.attribute.slug === 'artist');
      const artist = artistAttribute ? artistAttribute.values[0].name : '';
      const releaseYearAttribute = node.attributes.find((attribute: any) => attribute.attribute.slug === 'release-year');
      const releaseYear = releaseYearAttribute ? releaseYearAttribute.values[0].name : '';
      const startPrice = node.pricing.priceRange.start.gross.amount;
      const startCurrency = node.pricing.priceRange.start.gross.currency;
      const stopPrice = node.pricing.priceRange.stop.gross.amount;
      const stopCurrency = node.pricing.priceRange.stop.gross.currency;
      return {
        id: node.id,
        name: node.name,
        thumbnail: node.thumbnail,
        artist,
        releaseYear,
        startPrice,
        startCurrency,
        stopPrice,
        stopCurrency
      };
    });
    setFetchedProducts([...fetchedProducts, ...products]);
    const endCursor = data.products.edges[data.products.edges.length - 1]?.cursor;
    setAfter(endCursor);
  }

  const { loading, error, fetchMore } = useQuery(PRODUCTS_QUERY, getFetchQuery());

  useEffect(() => {
    if (typeof window !== 'undefined') {
      localStorage.setItem('filters', JSON.stringify(filters));
    }
    loadProducts();
  }, [filters]);

  const loadProducts = async ()  => {
    const { data } = await fetchMore(getFetchQuery());
    if (data) {
      setDataAfterFetch(data);
      if (isInitialLoad) {
        setIsInitialLoad(false);
      }
    }
  }

  if (loading && isInitialLoad) return <p>Loading...</p>;
  if (error) {
    console.error(error);
    return <p>Error: {error.message}</p>;
  };

  return (
    <div>
      <div className={styles.filters}>
        {Object.entries(options).map(([filterName, filterOptions]) => (
          <div key={filterName} className={styles.filter}>
            <Autocomplete
              placeholder={filterName}
              //@ts-ignore
              value={filters[filterName].map((value: string) => value)}
              onChange={(_, { value }) => handleFilterChange(filterName, [value])}
              options={filterOptions.map((option) => ({ label: option, value: option }))}
              sx={{ width: 150 }}
            />
          </div>
        ))}
        <div className={styles.filter}>
          <Input placeholder="Search..." variant="solid" value={filters.search} onChange={(e) => handleFilterChange("search", e.target.value)} />
        </div>
      </div>
      <div className={styles.sub}>
        <div className={styles.sub_right}>
          <div className={styles.in_stock}>
            <label>In Stock:</label>
            <Switch
              checked={filters.stockAvailability}
              onChange={(event) => handleFilterChange("stockAvailability", event.target.checked)}
            />
          </div>
          <div className={styles.clear}>
            <Button variant="solid" size="sm" onClick={handleClearFilters}>Clear All</Button>
          </div>
        </div>
      </div>
      {!isInitialLoad && <><div className={styles.product_list}>
        {fetchedProducts.map((product: Product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </div>
        <div className={styles.bottom}>
          <Button variant="solid" size="lg" onClick={loadProducts}>Load More</Button>
        </div>
      </>}
    </div>
  );
};

export default ProductList;
