import { useQuery } from "@apollo/client";
import { PRODUCT_DETAILS_QUERY } from "@/graphql/queries";
import { useEffect, useState } from "react";
import styles from './ProductDetail.module.scss'
import { useRouter } from 'next/navigation'
import Button from '@mui/joy/Button';

const ProductDetail = ({ productId }: { productId: string }) => {
  const { loading, error, data } = useQuery(PRODUCT_DETAILS_QUERY, {
    variables: { productId },
  });

  const [product, setProduct] = useState({
    name: "",
    images: [{ url: "" }],
    variants: [{
      id: "",
      name: "",
      pricing: {
        price: {
          gross: {
            amount: 0,
            currency: "USD"
          }
        }
      }
    }]
  });

  const router = useRouter();

  useEffect(() => {
    if (data) {
      setProduct(data.product);
    }
  }, [data]);

  const handleAddToBag = (id: string) => {
    alert(`Add to bag clicked! ID: ${id}`);
  }

  const handleBack = () => {
    router.push("/product/list");
  }

  if (loading) return <p>Loading...</p>;
  if (error) {
    console.error(error);
    return <p className={styles.error}>Error: {error.message}</p>;
  };

  return (
    <div className={styles.productDetail}>
      <Button className={styles.backButton} variant="solid" onClick={handleBack}>Back to product list</Button>
      <h1>{product.name}</h1>
      <div className={styles.blurb}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
      <div className={styles.imageGallery}>
        {product.images.map((image, index) => (
          <img key={index} src={image.url} alt={product.name} />
        ))}
      </div>
      {product.variants.map((variant) => (
        <div key={variant.id} className={styles.variant}>
          <h2>{variant.name}</h2>
          <p className={styles.price}>
            Price: {variant.pricing.price.gross.amount} {variant.pricing.price.gross.currency}
          </p>
          <Button className={styles.addButton} variant="solid" onClick={() => handleAddToBag(variant.id)}>Add to bag</Button>
        </div>
      ))}
    </div>
  );
};

export default ProductDetail;