"use client"

import React, { useState } from 'react';
import styles from './TopBar.module.scss';
import { FaSearch, FaPhone, FaHeart, FaUser, FaShoppingBag, FaBars } from 'react-icons/fa';

const TopBar = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  const menuItems = ["watches", "straps", "care", "subscription", "service", "stores", "about"];
  const iconComponents = [FaSearch, FaPhone, FaHeart, FaUser, FaShoppingBag];
  const iconNames = ["search", "phone", "heart", "user", "shopping-bag"];

  return (
    <div className={styles.topBar}>
      <div className={styles.menuContainer}>
        <img src="/breitling-inline.svg" alt="Breitling logo" className={styles.logo} />
        <div className={styles.menuToggle} onClick={() => setMenuOpen(!menuOpen)}>
          <FaBars />
        </div>
      </div>
      <nav className={`${styles.nav} ${menuOpen ? styles.open : ''}`}>
        {menuItems.map((item) => (
          <a key={item} href={`/${item}`}>{item}</a>
        ))}
      </nav>
      <div className={`${styles.icons} ${menuOpen ? styles.open : ''}`}>
        {iconComponents.map((Icon, index) => (
          <a key={iconNames[index]} href={`#${iconNames[index]}`} aria-label={iconNames[index]}><Icon /></a>
        ))}
      </div>
    </div>
  );
};


export default TopBar;
