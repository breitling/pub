"use client"

import { Product } from '@/models/product';
import AspectRatio from '@mui/joy/AspectRatio';
import Card from '@mui/joy/Card';
import CardContent from '@mui/joy/CardContent';
import CardOverflow from '@mui/joy/CardOverflow';
import Divider from '@mui/joy/Divider';
import Typography from '@mui/joy/Typography';
import Link from 'next/link';

interface ProductCardProps {
  product: Product
}

const ProductCard = ({ product }: ProductCardProps) => {
  const maxChars = 30;

  return (
    <Link href={`/product/detail/${product.id}`}>
      <Card variant="outlined" sx={{ width: 280 }}>
        <CardOverflow>
          <AspectRatio ratio="1">
            <img
              src={product.thumbnail.url}
              loading="lazy"
              alt={product.name}
            />
          </AspectRatio>
        </CardOverflow>
        <CardContent>
          <Typography level="h2" fontSize="sm">
            {product.name.length > maxChars ? product.name.slice(0, maxChars) + '...' : product.name}
          </Typography>
          <Typography level="body2" sx={{ mt: 0.5 }}>
            {product.artist}
          </Typography>
        </CardContent>
        <CardOverflow variant="soft" sx={{ bgcolor: 'background.level1' }}>
          <Divider inset="context" />
          <CardContent orientation="horizontal">
            <Typography level="body3" fontWeight="md" textColor="text.secondary">
              Year: {product.releaseYear}
            </Typography>
            <Divider orientation="vertical" />
            <Typography level="body3" fontWeight="md" textColor="text.secondary">
              {product.startPrice} {product.startCurrency} to {product.stopPrice} {product.stopCurrency}
            </Typography>
          </CardContent>
        </CardOverflow>
      </Card>
    </Link>
  );
};

export default ProductCard;
