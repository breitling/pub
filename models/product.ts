export interface Product {
  id: string;
  name: string;
  thumbnail: { url: string };
  artist: string;
  releaseYear: string;
  startPrice: number;
  startCurrency: string;
  stopPrice: number;
  stopCurrency: string;
}
